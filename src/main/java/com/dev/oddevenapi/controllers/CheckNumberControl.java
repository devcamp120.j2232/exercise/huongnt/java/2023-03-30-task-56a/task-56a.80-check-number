package com.dev.oddevenapi.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class CheckNumberControl {
    @GetMapping("/checknumber")
    public String checknumberApi(@RequestParam int inputnumber) {
        String returnMessage = new String();
        if (inputnumber % 2 == 0) {
            returnMessage = (inputnumber + " is even");
        } else
            returnMessage = (inputnumber + " is odd");

        return returnMessage;
    }

}
